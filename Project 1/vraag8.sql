create or replace PROCEDURE BEREKEN_TOTAL_GRADE(
    p_student_id IN students.student_id%TYPE,
    p_section_id IN sections.section_id%TYPE
) IS
    v_final_grade enrollments.final_grade%TYPE;
BEGIN
    SELECT ROUND(SUM(gem_numeric_grade) / COUNT(gem_numeric_grade)) INTO v_final_grade
    FROM v_grade_per_type
    WHERE student_id = p_student_id AND section_id = p_section_id;
    
    UPDATE enrollments
    SET final_grade = v_final_grade
    WHERE student_id = p_student_id AND section_id = p_section_id;
    
    COMMIT;
    
    DBMS_OUTPUT.PUT_LINE('De student ' || p_student_id || ' heeft voor de sectie ' || p_section_id || 
        ' de score ' || v_final_grade || ' toegewezen gekregen.');
END BEREKEN_TOTAL_GRADE;
