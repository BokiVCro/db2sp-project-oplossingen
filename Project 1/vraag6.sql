create or replace FUNCTION AANTAL_INSCHRIJVINGEN(
    p_section_id IN sections.section_id%TYPE
) RETURN NUMBER IS
    v_aantal NUMBER;
BEGIN
    SELECT COUNT(*) INTO v_aantal
    FROM enrollments
    WHERE p_section_id=section_id;
    
    RETURN v_aantal;
END AANTAL_INSCHRIJVINGEN;
