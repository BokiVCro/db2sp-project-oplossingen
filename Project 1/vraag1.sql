CREATE OR REPLACE PROCEDURE VOEG_STUDENT_TOE(
    p_salutation        IN students.salutation%TYPE,
    p_first_name        IN students.first_name%TYPE,
    p_last_name         IN students.last_name%TYPE,
    p_street_address    IN students.street_address%TYPE,
    p_zip               IN students.zip%TYPE,
    p_phone             IN students.phone%TYPE
) IS
BEGIN
    INSERT INTO students
    VALUES(seq_student_id.NEXTVAL, p_salutation, p_first_name, p_last_name, p_street_address, p_zip,
        p_phone, USER, SYSDATE, USER, SYSDATE, USER, SYSDATE);
        
    DBMS_OUTPUT.PUT_LINE('Student nr. ' || seq_student_id.CURRVAL || ' is succesvol toegevoegd!');
END VOEG_STUDENT_TOE;
