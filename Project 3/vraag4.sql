create or replace PROCEDURE OVERZICHT_COURSE_SECTION( -- Alles werkt normaal, behalve de lettergrades
    p_course_no IN COURSES.COURSE_NO%TYPE
) IS
    v_lettergrade grade_conversions.letter_grade%TYPE;
    
    CURSOR c_over_course IS
        SELECT course_no, description
        FROM courses
        WHERE p_course_no=course_no;
    
    CURSOR c_over_sec (p_course_number IN courses.course_no%TYPE) IS
        SELECT s.section_id, s.start_date_time, i.first_name, i.last_name
        FROM sections s
        JOIN instructors i ON s.instructor_id=i.instructor_id
        WHERE TO_CHAR(s.start_date_time, 'yyyy') = '2017' AND p_course_number = s.course_no;
    
    CURSOR c_over_student (p_section_no IN sections.section_id%TYPE) IS
        SELECT st.student_id, st.last_name, e.final_grade
        FROM sections s
        JOIN enrollments e ON (s.section_id=e.section_id)
        JOIN students st ON (e.student_id=st.student_id)
        WHERE p_section_no=s.section_id;
BEGIN
    FOR r_over_course IN c_over_course LOOP
        DBMS_OUTPUT.PUT_LINE('Cursus ' || r_over_course.course_no || ' ' || r_over_course.description);
        DBMS_OUTPUT.PUT_LINE('___________________________________');
        DBMS_OUTPUT.PUT_LINE('');
        
        FOR r_over_sec IN c_over_sec(r_over_course.course_no) LOOP
            DBMS_OUTPUT.PUT_LINE(r_over_sec.section_id || ' ' || r_over_sec.start_date_time || ' ' || r_over_sec.first_name 
            || ' ' || r_over_sec.last_name);
            DBMS_OUTPUT.PUT_LINE('-------------------------------');
            DBMS_OUTPUT.PUT_LINE(' ');
            
            FOR r_over_student IN c_over_student(r_over_sec.section_id) LOOP
                /*  SELECT letter_grade INTO v_lettergrade
                FROM GRADE_CONVERSIONS
                WHERE r_over_student.final_grade <= max_grade;*/
            
                DBMS_OUTPUT.PUT_LINE(r_over_student.student_id || ' ' || r_over_student.last_name || ' ' || r_over_student.final_grade 
                || ' ' || v_lettergrade);
                DBMS_OUTPUT.PUT_LINE(' ');
            END LOOP;
        END LOOP;
    END LOOP;
END OVERZICHT_COURSE_SECTION;
